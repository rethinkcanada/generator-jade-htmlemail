'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');
var yosay = require('yosay');

var JadeHtmlemailGenerator = yeoman.generators.Base.extend({
  initializing: function () {
    this.pkg = require('../package.json');
  },

  prompting: function () {
    var done = this.async();

    // Have Yeoman greet the user.
    this.log(yosay(
      'Welcome to the great Jade Html-Email generator!'
    ));

    var prompts = [{
      type: 'input',
      name: 'emailTitle',
      message: 'What is the title of the message of your emails?',
      default: 'New Email'
    }, {
      type: 'input',
      name: 'domainProduction',
      message: 'What\'s your production domain?',
      default: 'http://www.mydomain.com/',
      validate: function (value) {
          // Trim input value
          var domain = value.replace(/^\s+/g, '').replace(/\s+$/g, '');
          // Check if domain isn't empty
          if (!domain) {
              return 'You need to provide a production domain';
          }
          // Check if domain is valid
          if (!domain.match(/^(https?:\/\/)([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/)) {
              return 'You need to provide a valid domain';
          }
          return true;
      },
      filter: function (value) {
          // Make sure domain ends with a trailing slash
          if (value[value.length - 1] !== '/') {
              return value + '/';
          }
          return value;

      }
    }, {
      type: 'list',
      name: 'emailService',
      message: 'Select the SMTP host to use when sending test emails',
      choices: [
          'Gmail',
          'DynectEmail',
          'hot.ee',
          'Hotmail',
          'iCloud',
          'mail.ee',
          'Mail.Ru',
          'Mailgun',
          'Mailjet',
          'Mandrill',
          'Postmark',
          'QQ',
          'SendGrid',
          'SES',
          'Yahoo',
          'yandex',
          'Zoho'
      ],
      default: 0
    }, {
            type: 'input',
            name: 'emailAuthUser',
            message: 'What\'s your SMTP server username?',
            validate: function (value) {
                // Trim input value and check if it's not mepty
                if (!value.replace(/^\s+/g, '').replace(/\s+$/g, '')) {
                    return 'You need to provide a SMTP server username';
                }
                return true;
            }
        }, {
            type: 'password',
            name: 'emailAuthPassword',
            message: 'What\'s your SMTP server passsword?',
            validate: function (value) {
                // Trim input value and check if it's not mepty
                if (!value.replace(/^\s+/g, '').replace(/\s+$/g, '')) {
                    return 'You need to provide a SMTP server passsword';
                }
                return true;
            }
        }];

    this.prompt(prompts, function (props) {
      this.emailTitle = props.emailTitle;
      this.domainProduction = props.domainProduction;
      this.emailService        = props.emailService;
      this.emailAuthUser       = props.emailAuthUser;
      this.emailAuthPassword   = props.emailAuthPassword;
      done();
    }.bind(this));
  },

  writing: {
    app: function () {
      this.dest.mkdir('app');
      this.dest.mkdir('app/jade-mixins');
      this.dest.mkdir('app/css');
      this.dest.mkdir('app/scss');
      this.dest.mkdir('app/scss/imports');
      this.dest.mkdir('app/img');
      this.dest.mkdir('app/img/common');
      this.dest.mkdir('app/img/common/desktop');
      this.dest.mkdir('app/img/common/mobile');
      this.dest.mkdir('app/img/common/desktop/2x');
      this.dest.mkdir('app/img/common/mobile/2x');
      this.dest.mkdir('app/img/eng');
      this.dest.mkdir('app/img/eng/desktop');
      this.dest.mkdir('app/img/eng/mobile');
      this.dest.mkdir('app/img/eng/desktop/2x');
      this.dest.mkdir('app/img/eng/mobile/2x');
      this.dest.mkdir('app/img/fr');
      this.dest.mkdir('app/img/fr/desktop');
      this.dest.mkdir('app/img/fr/mobile');
      this.dest.mkdir('app/img/fr/desktop/2x');
      this.dest.mkdir('app/img/fr/mobile/2x');
      this.dest.mkdir('app/test');
      this.dest.mkdir('app/test/visual');
      this.dest.mkdir('app/test/visual/screenshots');
      this.dest.mkdir('app/test/visual/screenshots/desktop');
      this.dest.mkdir('app/test/visual/screenshots/mobile');
      this.dest.mkdir('app/result');
      this.dest.mkdir('app/result/visual');
      this.dest.mkdir('app/result/visual/desktop');
      this.dest.mkdir('app/result/visual/mobile');

      this.src.copy('_package.json', 'package.json');

      // grunt file, that is edited from input
      this.template('_Gruntfile.js','Gruntfile.js');

      // jade files, that are edited from input
      this.template('_index_en.jade', 'app/index_en.jade');
      this.template('_index_fr.jade', 'app/index_fr.jade');
      this.src.copy('index_EXAMPLE.jade','app/index_EXAMPLE.jade');
    },

    projectfiles: function () {
      this.src.copy('editorconfig', '.editorconfig');
      this.src.copy('phantomcss-test.js', 'test/visual/pahntomcss-test.js');

      // jade files
      this.src.copy('jade-mixins/email-image.jade','app/jade-mixins/email-image.jade');
      this.src.copy('jade-mixins/email-metadata.jade','app/jade-mixins/email-metadata.jade');
      this.src.copy('jade-mixins/email-table.jade','app/jade-mixins/email-table.jade');
      this.src.copy('jade-mixins/user-mixins.jade','app/jade-mixins/user-mixins.jade');

      // scss files
      this.src.copy('scss/style_en.scss','app/scss/style_en.scss');
      this.src.copy('scss/style_fr.scss','app/scss/style_fr.scss');
      this.src.copy('scss/imports/_base.scss','app/scss/imports/_base.scss');
      this.src.copy('scss/imports/_base_media_query_mixins.scss','app/scss/imports/_base_media_query_mixins.scss');
      this.src.copy('scss/imports/_main.scss','app/scss/imports/_main.scss');
      this.src.copy('scss/imports/_media_query_mixin.scss','app/scss/imports/_media_query_mixin.scss');
      this.src.copy('scss/imports/_variables.scss','app/scss/imports/_variables.scss');

      //the image data json file
      this.src.copy('image-data.json','app/image-data.json');
    }
  },

  end: function () {
    this.installDependencies();
  }
});

module.exports = JadeHtmlemailGenerator;
