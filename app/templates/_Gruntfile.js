module.exports = function (grunt) {
  'use strict';

  //Project configuration.
  grunt.initConfig({
    //pkg: grunt.file.readJSON('package.json'),

    /**
     * Project Paths Configuration
     * ===============================
     */
    paths: {
      projectname: 'Test Jade Email',
        //images folder name
        images: 'img',
        //where to store built files
        build: 'build',
        //sources
        src: 'app',
        //the 'going to be compiled' jade file
        jadeSrcIndex: 'index_en.jade',
        //the 'going to be compiled' sass file
        sassSrc: 'style_en.scss',
        //main email file
        email: 'index.html',
        //enter here yout production domain
        buildDomain: '<%= domainProduction %>',
        //this is the default development domain
        devDomain: 'http://<%%= connect.options.hostname %>:<%%= connect.options.port %>/'
    },

    /**
     * Jade Compilation Tasks
     * ===============================
     */
    jade: {
      dev: {
        options: {
          data: {
            debug: true,
            pretty: true,
            data: require('./app/image-data.json'),
          }
        },
        files: {
          '<%%= paths.src %>/index.html': '<%%= paths.src %>/<%%= paths.jadeSrcIndex %>'
        }
      },
      build: {
        options:{
          data: {
            debug: false,
            pretty: true,
            data: require('./app/image-data.json'),
          }
        },
        files: {
          '<%%= paths.build %>/pre-compile.html': '<%%= paths.src %>/<%%= paths.jadeSrcIndex %>'
        }
      }
    },

    /**
     * SCSS Compilation Tasks
     * ===============================
     */
    sass: {
      dev: {
        options: {
          style: 'expanded',
        },
        files: {
          '<%%= paths.src %>/css/style.css': '<%%= paths.src %>/scss/<%%= paths.sassSrc %>'
        }
      },
      build: {
        options: {
          style: 'expanded',
        },
        files: {
          '<%%= paths.build %>/css/style.css': '<%%= paths.src %>/scss/<%%= paths.sassSrc %>'
        }
      }
    },

    /**
     * Watch Tasks
     * ===============================
     */

    watch: {
      sass: {
        files: ['<%%= paths.src %>/scss/**/*.scss'],
        tasks: ['sass:dev','replace:dev']
      },
      jade: {
        files: [
          '<%%= paths.src %>/<%%= paths.jadeSrcIndex %>',
          '<%%= paths.src %>/jade-mixins/{,*/}*.jade',
        ],
        tasks: ['jade:dev','replace:dev']
      },
      livereload: {
        options: {
            livereload: '<%%= connect.options.livereload %>'
        },
        files: [
            '<%%= paths.src %>/<%%= paths.email %>',
            '<%%= paths.src %>/css/{,*/}*.css',
            '<%%= paths.src %>/<%%= paths.images %>/{,*/}*.{png,jpg,jpeg,gif}'
        ]
      }
    },

    /**
     * Premailer Parser Tasks
     * ===============================
     */
    premailer: {
        build: {
            src: '<%%= paths.build %>/pre-compile.html',
            dest: '<%%= paths.build %>/<%%= paths.email %>',
            ioException: true
        }
    },

    cssmin: {
      minify: {
        src: ['<%%= paths.build %>/<%%= paths.email %>'],
        dest: '<%%= paths.build %>/<%%= paths.email %>'
      }
    },

    /**
     * Server Tasks
     * ===============================
     */
    connect: {
      options: {
          open: '<%%= paths.devDomain %>',
          hostname: 'localhost',
          livereload: 35729,
          port: 8022
      },
      dev: {
          options: {
              base: '<%%= paths.src %>'
          }
      },
      build: {
        options: {
            base: '<%%= paths.build %>'
        }
      }
    },

    /**
     * Cleanup Tasks
     * ===============================
     */
    clean: {
        buildStart: ['<%%= paths.build %>/index.html'],
        buildFinish: ['<%%= paths.build %>/css/','<%%= paths.build %>/pre-compile.html'],
        buildImage: ['<%%= paths.build %>/img/']
    },

    /**
     * Replacing Tasks
     * (use this to add the absolute path of images for build)
     * ===============================
     */
    replace: {
      dev: {
        options: {
          patterns: [
            {
              match: 'img/',
              replacement: '../img/'
            }
          ]
        },
        files: [
          {
            expand: true,
            src: ['<%%= paths.src %>/index.html', '<%%= paths.src %>/css/style.css']
          }
        ]
      },
        build: {
          options: {
            patterns: [
              {
                match: 'img/',
                replacement: '<%%= paths.buildDomain %>/img/'
              }
            ]
          },
          files: [
            {
              expand: true,
              src: '<%%= paths.build %>/index.html'
            }
          ]
        }
    },

    /**
     * Image Downsizing Tasks
     * ===============================
     */
     image_resize: {
      onlynew: {
        options: {
          width: '50%',
          height: '50%',
          quality: 1,
          overwrite: true
        },
        files: [{
          expand: true,
          cwd: '<%%= paths.src %>/img/eng/desktop/2x',
          src: '*',
          dest: '<%%= paths.src %>/img/eng/desktop/'
        }, {
          expand: true,
          cwd: '<%%= paths.src %>/img/eng/mobile/2x',
          src: '*',
          dest: '<%%= paths.src %>/img/eng/mobile/'
        },{
          expand: true,
          cwd: '<%%= paths.src %>/img/fr/desktop/2x',
          src: '*',
          dest: '<%%= paths.src %>/img/fr/desktop/'
        },{
          expand: true,
          cwd: '<%%= paths.src %>/img/fr/mobile/2x',
          src: '*',
          dest: '<%%= paths.src %>/img/fr/mobile/'
        },{
          expand: true,
          cwd: '<%%= paths.src %>/img/common/desktop/2x',
          src: '*',
          dest: '<%%= paths.src %>/img/common/desktop/'
        },{
          expand: true,
          cwd: '<%%= paths.src %>/img/common/mobile/2x',
          src: '*',
          dest: '<%%= paths.src %>/img/common/mobile/'
        }]
      }
     },

     /**
      * Image Optimize build Task
      * ===============================
      */
     imageoptim: {
      build:{
        options: {
          jpegMini: false,
          imageAlpha: false,
          quitAfter: false
        },
        src: ['<%%= paths.src %>/img/**/*.jpg']
      }
     },

     /**
      * Copy build Task
      * ===============================
      */

    copy: {
      build: {
        files: [
        {
          expand: true,
          cwd: '<%%= paths.src %>/img/',
          src: '**/*',
          dest: '<%%= paths.build %>/img/'
          }
        ]

      }
    },

     /**
      * Test Mailer Tasks
      * ===============================
      */

     nodemailer: {
      options: {
        transport: {
          type: 'SMTP',
          options: {
            service: '<%= emailService %>',
            auth: {
              user: '<%= emailAuthUser %>',
              pass: '<%= emailAuthPassword %>'
            }
          }
        }
      },
      send: {
          src: ['<%%= paths.build %>/<%%= paths.email %>']
      }
     },

     /**
      * Notification Tasks
      * ===============================
      */
     notify: {
      notify_hooks: {
        options: {
          enabled: true,
          title: '<%%= projectname %>'
        }
      },
      dev: {
        options: {
          message: 'Dev Environment Ready!'
        }
      }
     },

     /**
      * PhantomCSS
      * ===============================
      */
     phantomcss: {
      desktop: {
        options: {
          screenshots: 'test/visual/screenshots/desktop/',
          results: 'results/visual/desktop',
          viewportSize: [1024,768],
          logLevel: 'warning',
          force: true
        },
        src: [
          'test/visual/**.js'
        ]
      },
      mobile: {
        options: {
          screenshots: 'test/visual/screenshots/mobile/',
          results: 'results/visual/mobile',
          viewportSize: [320,480],
          logLevel: 'warning',
          force: true
        },
        src: [
          'test/visual/**.js'
        ]
      }
     },
  });

  [
    'grunt-contrib-connect',
    'grunt-contrib-watch',
    'grunt-contrib-sass',
    'grunt-contrib-jade',
    'grunt-contrib-clean',
    'grunt-contrib-copy',
    'grunt-imageoptim',
    'grunt-premailer',
    'grunt-contrib-cssmin',
    'grunt-replace',
    'grunt-image-resize',
    'grunt-notify',
    'grunt-nodemailer',
    'grunt-phantomcss',

  ].forEach(grunt.loadNpmTasks);

  grunt.registerTask('default', 'dev');

  grunt.registerTask('dev',[
    'jade:dev',
    'sass:dev',
    'replace:dev',
    'connect:dev',
    'notify:dev',
    'watch',
  ]);

  grunt.registerTask('normalizeimage', [
    'image_resize'
  ]);

  grunt.registerTask('buildimage', [
    'imageoptim:build'
  ]);

  grunt.registerTask('build',[
    'clean:buildStart',
    'clean:buildImage',
    'copy:build',
    'jade:build',
    'sass:build',
    'premailer:build',
    'replace:build',
    'cssmin',
    'clean:buildFinish'
  ]);

  grunt.registerTask('build:diff',[
    'clean:buildStart',
    'clean:buildImage',
    'copy:build',
    'jade:build',
    'sass:build',
    'premailer:build',
    'replace:build',
    'cssmin',
    'clean:buildFinish',
    'forceEnabled',
    'phantomcss:desktop',
    'phantomcss:mobile'
  ]);

  grunt.registerTask('forceEnabled','diff tool with force enabled',
    function() {
      grunt.option('force', true);
    }
  );

  grunt.registerTask('build:view',[
    'build',
    'connect:build',
    'watch'
  ]);

  grunt.registerTask('send', 'Send to litmus test',
    function(n) {
      var target = grunt.option('e');
      grunt.config.set('nodemailer.options.recipients', [{name: 'litmus', email: target}]);
      grunt.task.run('nodemailer:send');
      grunt.log.ok('Remember to refer to this css guide:');
      grunt.log.ok('https://www.campaignmonitor.com/css/');
    }
  );
}
