# generator-jade-htmlemail

Please feel free to improve this generator!

To install generator-jade-htmlemail from npm, run:

```bash
npm install -g generator-jade-htmlemail
```

Initiate the generator:

```bash
yo jade-htmlemail
```

#Usage
Some grunt tasks build upon the [generator-htmlemail](https://github.com/jahvi/generator-htmlemail). But was not organized for resolution independent screens [Desktop/Mobile], and was really hard to read tables, due to the fact that there end up being so many nested levels of tables, rows and cells.

Most of your work will be in the following files, the english version will have example code to get you started:
```bash
	.
	+-- index_en.jade <<<<<<<<<<<<< language spec. markup
	+-- index_fr.jade
	+-- jade-mixins
	|	++- user-mixins.jade <<<<<< custom jade mixins
	+-- scss
	|	+-- style_en.scss <<<<<<<<< language spec. styles and MQs
	|	+-- style_fr.scss
	|	+-- imports
	|	|	+-- _variables.scss <<< reusable variables
	|	|	+-- _main.scss <<<<<<<< common styles and media queries
	+-- img     <<<<<<<<<< normal and retina sized images
	|	+-- common
	|	|	+-- desktop
	|	|	|	+-- 2x
	|	|	+-- mobile
	|	|	|	+-- 2x
	|	+-- eng
	|	|	+-- desktop
	|	|	|	+-- 2x
	|	|	+-- mobile
	|	|	|	+-- 2x
	|	+-- fr
	|	|	+-- desktop
	|	|	|	+-- 2x
	|	|	+-- mobile
	|	|	|	+-- 2x
	
```
#### Note:
You may want to quickly review how [Jade](jade-lang.com) works first, it's easy to pick up for the use of making email using this generator.

For an example use of the mixins and markup, refer to the file index_EXAMPLE.jade in the generated project.
## How it works
The project is split up into two languages, English and French. This sets the organization of the project for even more langauges if need be. 

Retina sized images should be organized and placed into the '2x' marked file, for the appropriate view size. The grunt task "grunt normalizeimage"  is defined to automate the downscaling of the image sizes by half, and places them into the directory one step up locally.

For organization purposes within the markup, the images data is seperated into a json file called 'image-data.json', that is read by the grunt task and sends it to your targeted index.jade file. The variables are pre-defined in the index.jade file, and should be used in conjunction with the mixin helper "+image(imgData.imageKey)". An example is provided in the jade file.

The targeted index.jade file has additional, predefined mixins helpers imported, they are located in the jade-mixins folder. They include helpers for creating tables, tds, and retina/regular screen image markup. Examples are provided in the jade file.

This project generator uses the precompiler Sass for CSS. Because responsive emails are media query dependent and require a slight change as to how they are written for some email clients (yahoo! mail), a special mixin for creating media queries is predefined. Using the media query mixin also allows you to organize your selector specific media query, but when the Sass files are compiled, the media queries are merged under common breaking points for optimization. 

Although the normal sized images are defined in the image-data.json file, they must be defined in width and height again within your media queries. (At this time, there was no way for sass to read and iterate over json data). However, please use idential selectors names defined in your image-data.json file. Again, examples are provided in the sass files.


## The Why?

This generator has been the result of many frustrations with the standard generator-htmlemail generator that I felt were missing/lacking.

### Why use Jade?

Jade is able to use mixins to summarize long and tedious element tags and attributes (DRY coding); in email, each tables requires quite a number of attributes that must be included. Using mixins also prevents mis-spelling, and trying to find where you may have made the mistake.
 
 So instead of this on every table & td declaration

```
#!html
<table cellpadding="0" cellspacing="0" border="0">
   <tr>
      <td cellpadding="0" cellspacing="0" border="0">
         <p>Hello World</p>
      </td>
   </tr>
</table>
```

 You use this instead...

```
#!jade
  +table
    tr
      +cell
         p Hello World
```

Jade uses indentations to indicate a child node. 
When you have tables, with say 20 child rows nodes, 30 cells child nodes, with even more table child nodes, it becomes very common to miss adding in a closing tag; Finding that missing tag can be extreamly hard. Also, if you by mistake misposition the parent/child older in html, you would need to find the correct corrisponding closing tag. In Jade, you just move and indent accordingly.

Jade can have json data passed into it during the compiling process.
Dealing with images is also a hassle and can be difficult to find the one image you are looking for in an ocean of tags. Separating the image's data (width,height,src,alt,etc.) into a json data keeps it nice and tidy, and easily accessable.

Jade removes all trailing spaces on build.
Some email clients will break your layout if you have any spaces inside your td tags; Finding that one trailing space is a time waster. You don't need to worry about it when using Jade.


### Grunt Tasks

```bash
  $ grunt
  //default dev enviroment with live reload.
  
  $ grunt build
  //creates an index.html file in the 'build' directory,
  //inlining the classes, and including all css selectors
  //and media queries.
  
  $ grunt build:diff
  //same as the above task, this task takes a snapshot
  //of both the desktop and mobile version and checks
  //for any major changes you may be unaware of.
  //"Don't guess, Test!"
  
  $ grunt build:view
  //this task is identical to 'grunt build', but 
  //opens a browser window to view the built index.html
  
  $ grunt send -e email@gmail.com
  //sends the built index.html file to a destination email
  //address. This task was created mainly for litmus email 
  //tests; the email testing address changes each time 
  //you run a new test.
  
  $ grunt buildimage
  //optimizes all images in the app(dev) directory 
  //using imageoptim.
  
  $ grunt normalizeimage
  //creates new images from the images located in the 
  //2x folders. Reduces the image size by 50%, and creates
  //a new image file in the directory above it's original
  //location.
```

If you'd like to get to know Yeoman better and meet some of his friends, [Grunt](http://gruntjs.com) and [Bower](http://bower.io), check out the complete [Getting Started Guide](https://github.com/yeoman/yeoman/wiki/Getting-Started).


## License

MIT